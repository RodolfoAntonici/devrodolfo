//
//  Utils.swift
//  DribbleParse
//
//  Created by Rodolfo Antonici on 9/18/15.
//  Copyright © 2015 Rodolfo Antonici. All rights reserved.
//

import UIKit

let UserInterfaceIdiom = UIDevice.currentDevice().userInterfaceIdiom

extension String {
    static func htmlToString(htmlText: String) -> String? {

        do {
            return try NSAttributedString(data: htmlText.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil).string
        } catch {
            return nil
        }
        
    }
}

    

