//
//  DetailViewController.swift
//  DribbleParse
//
//  Created by Rodolfo Antonici on 9/24/15.
//  Copyright © 2015 Rodolfo Antonici. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var shot: Shot? 
    @IBOutlet var tableView: UITableView!
    @IBOutlet var backgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.backgroundView?.backgroundColor = UIColor.clearColor()
        self.tableView.contentInset = UIEdgeInsets(top: self.backgroundImageView.frame.height+64, left: 0, bottom: 0, right: 0
        )
        self.tableView.contentOffset = CGPoint(x: 0, y: -(self.backgroundImageView.frame.height+64))

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if let unwrapedShot = self.shot {
            
            if let unwrapedImageURL = unwrapedShot.imageURL {
                
                self.backgroundImageView.sd_setImageWithURL(NSURL(string: unwrapedImageURL)!, placeholderImage: UIImage(named: "dribbble"))
            }

            self.tableView.reloadData()
        }
    }
//MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("TextCell") as! TextTableViewCell
            guard let unwrapedShot = self.shot else {
                return cell
            }
            if let simpleText = String.htmlToString(unwrapedShot.description!) {
                cell.descriptionLabel.text = simpleText
                cell.descriptionLabel.sizeToFit()
            }
            return cell
            
        default:
            ()
        }
        
        return UITableViewCell()
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        guard let unwrapedShot = self.shot else {
            return 54
        }
        

        if let simpleText = String.htmlToString(unwrapedShot.description!) {

            let constraintSize = CGSizeMake(self.view.frame.width - 16, CGFloat.max)
            
            let attributes = [NSFontAttributeName: UIFont.systemFontOfSize(17.0)]
            let labelSize = simpleText.boundingRectWithSize(constraintSize,
                options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                attributes: attributes,
                context: nil)
            
            return labelSize.height + 16
        }
        

        return 54
    }
    
    
}

class TextTableViewCell: UITableViewCell {
    @IBOutlet var descriptionLabel: UILabel!
}