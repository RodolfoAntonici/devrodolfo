//
//  Models.swift
//  DribbleParse
//
//  Created by Rodolfo Antonici on 9/18/15.
//  Copyright © 2015 Rodolfo Antonici. All rights reserved.
//
//  Simple Models to deal with requests

import UIKit
import ObjectMapper

public class Shot: Mappable {
    var likesCount: Int?
    var teaserImageURL: String?
    var title: String?
    
    var imageURL: String?
    var description: String?
    
    required convenience public init?(_ map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        likesCount <- map["likes_count"]
        teaserImageURL <- map["image_teaser_url"]
        title <- map["title"]
        
        imageURL <- map["image_url"]
        description <- map["description"]
        
    }
}


