
//  ViewController.swift
//  DribbleParse
//
//  Created by Rodolfo Antonici on 9/18/15.
//  Copyright © 2015 Rodolfo Antonici. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SDWebImage




enum StoryboardSegue: String {
    case Detail = "DetailSegue"
}
class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ViewControllerViewModelDelegate {
    

    
    
// Properties:
    let viewModel = ViewControllerViewModel()
    var currentPage = 1 // Yeah, the Dribble API returns the same result at the page 0 and 1, so I'll start at the 1 index
    var refresher = UIRefreshControl()
//    Outlets:
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refresher.addTarget(self, action: "reloadData", forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView.addSubview(self.refresher)
        self.viewModel.delegate = self
        self.viewModel.loadPage(1)
        self.refresher.beginRefreshing()
    }
    
    func reloadData() {
        self.viewModel.loadPage(1)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let segueIdentifier = segue.identifier else {
            return
        }
        
        switch StoryboardSegue(rawValue: segueIdentifier)! {
        case StoryboardSegue.Detail:
            let detailViewController = segue.destinationViewController as! DetailViewController
            if let index = sender as? Int {
                
                detailViewController.shot = self.viewModel.currentShots[index]
            }
        }
    }
    
//MARK: ViewControllerViewModelDelegate
    
    func viewModel(viewModel: ViewControllerViewModel, didBeginLoadShotsForPage page: Int) {
//      Empty implementation, for now
    }
    
    func viewModel(viewModel: ViewControllerViewModel, didFinishLoadShotsForPage page: Int) {
        self.collectionView.reloadData()
        if self.refresher.refreshing {
            self.refresher.endRefreshing()
        }
    }
    
    func viewModel(viewModel: ViewControllerViewModel, didFailLoadShotsForPage page: Int) {
        if self.refresher.refreshing {
            self.refresher.endRefreshing()
        }
    }
//MARK: UICollectionViewDataSource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return self.viewModel.currentShots.count == 0 ? 1 : self.viewModel.currentShots.count
        return self.viewModel.currentShots.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("DribbleCell", forIndexPath: indexPath) as! DribbleCell
        cell.shot = self.viewModel.currentShots[indexPath.row]
        return cell
    }
    
//MARK: UICollectionViewDelegateFlowLayout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 300, height: 220)
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
//        Why I should get it every time instead of fixing the values... well, that is simple, if I change it because of something, I'll not have to change in every place I've used it
        let cellSize = self.collectionView(self.collectionView, layout: self.collectionView.collectionViewLayout, sizeForItemAtIndexPath: NSIndexPath(forItem: 0, inSection: 0))
        let cellSpacing = self.collectionView(self.collectionView, layout: self.collectionView.collectionViewLayout, minimumLineSpacingForSectionAtIndex: 0)
        
//        print("\((scrollView.contentOffset.y + scrollView.frame.size.height) / (cellSize.height + cellSpacing))")
        let currentBottomShotNumber = Int(round((scrollView.contentOffset.y + scrollView.frame.size.height) / (cellSize.height + cellSpacing)))
        
        if currentBottomShotNumber > self.viewModel.currentPageNumber * 15 - 3 {
            //loadNextpage
            self.viewModel.loadPage(self.viewModel.currentPageNumber + 1)
        }
    }
    
//MARK: UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("DetailSegue", sender: indexPath.row)
    }
}

protocol ViewControllerViewModelDelegate {
    func viewModel(viewModel: ViewControllerViewModel, didFinishLoadShotsForPage page: Int)
    func viewModel(viewModel: ViewControllerViewModel, didBeginLoadShotsForPage page: Int)
    func viewModel(viewModel: ViewControllerViewModel, didFailLoadShotsForPage page: Int)
}

// Just a simple class that will deal with requests and the ViewController data.
class ViewControllerViewModel {
    var delegate: ViewControllerViewModelDelegate?
    var currentShots = [Shot]()
    var lastPage = 50 //    50 is the default value that the API provides, but I'll let it var, so if the api changes the app won't breaks
    var currentPageNumber = 1
    var isLoadingNewPage = false
    
//              Note that page 0 it's the same as page 1
    func loadPage(page: Int) {
        if page <= self.currentPageNumber {
            self.isLoadingNewPage = false
        }
        
        if self.isLoadingNewPage == false && page <= self.lastPage{
            print("loadNextPage")
            self.isLoadingNewPage = true
            
            if let unwrapedDelegate = self.delegate {
                unwrapedDelegate.viewModel(self, didBeginLoadShotsForPage: page)
            }
            Alamofire.Manager.sharedInstance.request(.GET, NSURL(string: "https://api.dribbble.com/shots/popular?page=\(page)")!).responseJSON { (request, response, result) -> Void in
                
                if result.isSuccess {
                    if let valueDictionary = result.value as? Dictionary<String, AnyObject> {
                        self.lastPage = valueDictionary["pages"] as! Int
                        //                    var shotsArray = valueDictionary["shots"]
                        
                        var shotsArray = [Shot]()
                        for currentShot in valueDictionary["shots"] as! Array<AnyObject>{
                            
                            if let shot = Mapper<Shot>().map(currentShot) {
                                
                                shotsArray.append(shot)
                            }
                        }
                        
                        
                        if self.currentPageNumber + 1 == page {
                            self.currentShots.appendContentsOf(shotsArray)
                        }
                        else {
                            self.currentShots = shotsArray
                        }
                        
                        self.currentPageNumber = page
                        
                        if let unwrapedDelegate = self.delegate {
                            unwrapedDelegate.viewModel(self, didFinishLoadShotsForPage: page)
                        }
                    }
                    
                }
                else {
                    if let unwrapedDelegate = self.delegate {
                        unwrapedDelegate.viewModel(self, didFailLoadShotsForPage: page)
                    }
                }
                
                self.isLoadingNewPage = false
            }
        }
    }
}




class DribbleCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var viewsNumberLabel: UILabel!
    @IBOutlet var eyeImageView: UIImageView!
    
    var shot: Shot? {
        didSet {
            guard let unwrapedShot = self.shot else {
                return
            }
            self.titleLabel.text = unwrapedShot.title
            if let unwrapedTeaserImageURL = unwrapedShot.teaserImageURL {
                
                self.imageView.sd_setImageWithURL(NSURL(string: unwrapedTeaserImageURL)!, placeholderImage: UIImage(named: "dribbble"))
            }
            
            if let unwrapedLikesCount = unwrapedShot.likesCount {
                
                self.viewsNumberLabel.text = "\(unwrapedLikesCount)"
            }
            
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.eyeImageView.image = self.eyeImageView.image!.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.eyeImageView.tintColor = UIColor.whiteColor()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = UIImage(named: "dribbble")
        self.viewsNumberLabel.text = ""
        self.titleLabel.text = ""
    }
    
}